package com.mi.game;

public class Grid {
	
	private int[][] array = new int[][] {
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{0, 0, 0, 0},
			{0, 0, 0, 0}
	};
	
	public boolean isFree(int x, int y) {
		return array[y][x] == 0;
	}
	
	public int getNumber(int x, int y) {
		return array[y][x];
	}
	
	public void setNumber(int x, int y, int number) {
		array[y][x] = number;
	}
}
