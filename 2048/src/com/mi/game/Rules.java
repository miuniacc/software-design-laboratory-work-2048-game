package com.mi.game;

import java.util.Scanner;

public class Rules {
	
	private Grid grid;
	private int score;
	private Boolean changed;
	
	public Rules(Grid grid) {
		this.grid = grid;
	}
	
	public void processCommand(Command command, int score) {
		this.score = score;
		changed = false;
		switch(command) {
		case LEFT:
			moveTilesLeft();
			break;
		case RIGHT:
			moveTilesRight();
			break;
		case UP:
			moveTilesUp();
			break;
		case DOWN:
			moveTilesDown();
			break;
		case QUIT:
			endTheGame();
			break;
		case NOTHING:
			break;
		default:
			break;
		}
	}
	
	private void moveTilesLeft() {
		int step = 0;
		for(int y = 0; y < 4; y++) {
			int x = 1;
			while(x < 4) {
				if(x == step) {
					x++;
				}
				else if(!grid.isFree(x, y)){
					if(grid.isFree(step, y)) {
						grid.setNumber(step, y, grid.getNumber(x, y));
						grid.setNumber(x, y, 0);
						changed = true;
						x++;
					}
					else if(grid.getNumber(x, y) == grid.getNumber(step, y)) {
						grid.setNumber(step, y, grid.getNumber(x, y) * 2);
						this.score += grid.getNumber(x, y) * 2;
						grid.setNumber(x, y, 0);
						changed = true;
						step++;
						x++;
					}
					else {
						step++;
					}
				}
				else {
					x++;
				}
			}
			step = 0;
		}
	}
	
	private void moveTilesRight() {
		int step = 3;
		for(int y = 0; y < 4; y++) {
			int x = 2;
			while(x > -1) {
				if(x == step) {
					x--;
				}
				else if(!grid.isFree(x, y)){
					if(grid.isFree(step, y)) {
						grid.setNumber(step, y, grid.getNumber(x, y));
						grid.setNumber(x, y, 0);
						changed = true;
						x--;
					}
					else if(grid.getNumber(x, y) == grid.getNumber(step, y)) {
						grid.setNumber(step, y, grid.getNumber(x, y) * 2);
						this.score += grid.getNumber(x, y) * 2;
						grid.setNumber(x, y, 0);
						changed = true;
						step--;
						x--;
					}
					else {
						step--;
					}
				}
				else {
					x--;
				}
			}
			step = 3;
		}
	}
	
	private void moveTilesUp() {
		int step = 0;
		for(int x = 0; x < 4; x++) {
			int y = 1;
			while(y < 4) {
				if(y == step) {
					y++;
				}
				else if(!grid.isFree(x, y)){
					if(grid.isFree(x, step)) {
						grid.setNumber(x, step, grid.getNumber(x, y));
						grid.setNumber(x, y, 0);
						changed = true;
						y++;
					}
					else if(grid.getNumber(x, y) == grid.getNumber(x, step)) {
						grid.setNumber(x, step, grid.getNumber(x, y) * 2);
						this.score += grid.getNumber(x, y) * 2;
						grid.setNumber(x, y, 0);
						changed = true;
						step++;
						y++;
					}
					else {
						step++;
					}
				}
				else {
					y++;
				}
			}
			step = 0;
		}
	}
	
	private void moveTilesDown() {
		int step = 3;
		for(int x = 0; x < 4; x++) {
			int y = 2;
			while(y > -1) {
				if(y == step) {
					y--;
				}
				else if(!grid.isFree(x, y)){
					if(grid.isFree(x, step)) {
						grid.setNumber(x, step, grid.getNumber(x, y));
						grid.setNumber(x, y, 0);
						changed = true;
						y--;
					}
					else if(grid.getNumber(x, y) == grid.getNumber(x, step)) {
						grid.setNumber(x, step, grid.getNumber(x, y) * 2);
						this.score += grid.getNumber(x, y) * 2;
						grid.setNumber(x, y, 0);
						changed = true;
						step--;
						y--;
					}
					else {
						step--;
					}
				}
				else {
					y--;
				}
			}
			step = 3;
		}
	}
	
	public Boolean isGameOver() {
		int count = 0;
		for(int x = 0; x < 4; x++) {
			for(int y = 0; y < 4; y++) {
				if(!grid.isFree(x, y)) {
					count++;
				}
			}
		}
		if(count == 16) {
			return !movesExist();
		}
		else {
			return false;
		}
	}
	
	private Boolean movesExist() {
		return checkNorthWestCorner() || checkNorthEastCorner() || checkSouthEastCorner() || checkSouthWestCorner() || checkNorthSide() ||
				checkEastSide() || checkWestSide() || checkSouthSide() || checkGridCenter();
	}
	
	private Boolean checkNorthWestCorner() {
		return grid.getNumber(0, 0) == grid.getNumber(1, 0) || grid.getNumber(0, 0) == grid.getNumber(0, 1);
	}
	
	private Boolean checkNorthEastCorner() {
		return grid.getNumber(3, 0) == grid.getNumber(2, 0) || grid.getNumber(3, 0) == grid.getNumber(3, 1);
	}
	
	private Boolean checkSouthEastCorner() {
		return grid.getNumber(3, 3) == grid.getNumber(2, 3) || grid.getNumber(3, 3) == grid.getNumber(3, 2);
	}
	
	private Boolean checkSouthWestCorner() {
		return grid.getNumber(0, 3) == grid.getNumber(1, 3) || grid.getNumber(0, 3) == grid.getNumber(0, 2);
	}
	
	private Boolean checkNorthSide() {
		for(int x = 1; x < 3; x++) {
			if(grid.getNumber(x, 0) == grid.getNumber(x+1, 0) || grid.getNumber(x, 0) == grid.getNumber(x-1, 0) ||
					grid.getNumber(x, 0) == grid.getNumber(x, 1)) {
				return true;
			}
		}
		return false;
	}
	
	private Boolean checkSouthSide() {
		for(int x = 1; x < 3; x++) {
			if(grid.getNumber(x, 3) == grid.getNumber(x+1, 3) || grid.getNumber(x, 3) == grid.getNumber(x-1, 3) ||
					grid.getNumber(x, 3) == grid.getNumber(x, 2)) {
				return true;
			}
		}
		return false;
	}
	
	private Boolean checkEastSide() {
		for(int y = 1; y < 3; y++) {
			if(grid.getNumber(3, y) == grid.getNumber(2, y) || grid.getNumber(3, y) == grid.getNumber(3, y-1) ||
					grid.getNumber(3, y) == grid.getNumber(3, y+1)) {
				return true;
			}
		}
		return false;
	}
	
	private Boolean checkWestSide() {
		for(int y = 1; y < 3; y++) {
			if(grid.getNumber(0, y) == grid.getNumber(1, y) || grid.getNumber(0, y) == grid.getNumber(0, y-1) ||
					grid.getNumber(0, y) == grid.getNumber(0, y+1)) {
				return true;
			}
		}
		return false;
	}
	
	private Boolean checkGridCenter() {
		for(int x = 1; x < 3; x++) {
			for(int y = 1; y < 3; y++) {
				if(grid.getNumber(x, y) == grid.getNumber(x+1, y) || grid.getNumber(x, y) == grid.getNumber(x-1, y) ||
						grid.getNumber(x, y) == grid.getNumber(x, y+1) || grid.getNumber(x, y) == grid.getNumber(x, y-1)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public Boolean goalReached() {
		for(int x = 0; x < 4; x++) {
			for(int y = 0; y < 4; y++) {
				if(grid.getNumber(x, y) == 2048) {
					return true;
				}
			}
		}
		return false;
	}
	
	public void win() {
		System.out.println("You win. Want to quit? (y/n)");
		Scanner input = new Scanner(System.in);
		char choise = input.next().charAt(0);
		input.close();
		if(choise == 'y' || choise == 'Y') {
			endTheGame();
		}
	}
	
	public int getScore() {
		return score;
	}
	
	public Boolean gridChanged() {
		return changed;
	}
	
	public void endTheGame() {
		System.out.println("Game over! Final score: " + score);
		System.exit(0);
	}
}
