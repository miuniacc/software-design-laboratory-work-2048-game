package com.mi.game;

import java.util.Random;

public class Generator {
	private Grid grid;
	
	public Generator(Grid grid) {
		this.grid = grid;
	}
	
	public void generate(int turn) {
		Random random = new Random();
		int number;
		int[] coordinates = new int[2];
		if(isPerimeterFree()) {
			generate(coordinates);
			while(!grid.isFree(coordinates[0], coordinates[1])) {
				generate(coordinates);
			}
		}
		else {
			generateInTheCenter(coordinates);
		}
		if(turn == 1) {
			number = 2;
		}
		else {
			number = (random.nextInt(2) + 1) * 2; // either 2 or 4
		}
		grid.setNumber(coordinates[0], coordinates[1], number);
	}
	
	private void generate(int[] coordinates) {
		Random random = new Random();
		coordinates[0] = random.nextInt(4);
		if(coordinates[0] == 0 || coordinates[0] == 3) {
			coordinates[1] = random.nextInt(4);
		}
		else {
			coordinates[1] = random.nextInt(2) * 3; // either 0 or 3
		}
	}
	
	private void generateInTheCenter(int[] coordinates) {
		Random random = new Random();
		coordinates[0] = random.nextInt(4);
		coordinates[1] = random.nextInt(4);
		while(!grid.isFree(coordinates[0], coordinates[1])) {
			coordinates[0] = random.nextInt(4);
			coordinates[1] = random.nextInt(4);
		}

	}
	
	private Boolean isPerimeterFree() {
		int notFree = 0;
		for(int x = 0; x < 4; x++) {
			for(int y = 0; y < 4; y++) {
				if((!(x == 1 || x == 2) && !(y == 1 || y == 2)) && !grid.isFree(x, y)) {
					notFree++;
				}
			}
		}
		return notFree != 12;
	}
}
