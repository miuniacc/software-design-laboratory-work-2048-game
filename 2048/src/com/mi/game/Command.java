package com.mi.game;

public enum Command {
	LEFT,
	RIGHT,
	UP,
	DOWN,
	QUIT,
	NOTHING
}
