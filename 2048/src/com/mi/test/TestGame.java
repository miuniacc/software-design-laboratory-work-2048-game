package com.mi.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.mi.game.Command;
import com.mi.game.Grid;
import com.mi.game.Rules;

public class TestGame {
	
	private Grid grid = new Grid();
	private Rules rules = new Rules(grid);
	
	@Before
	public void clearGrid() {
		for(int x = 0; x < 4; x++) {
			for(int y = 0; y < 4; y++) {
				grid.setNumber(x, y, 0);
			}
		}
	}
	
	private void createDummy1() {
		grid.setNumber(0, 0, 4);
		grid.setNumber(1, 0, 2);
		grid.setNumber(2, 0, 2);
		
		grid.setNumber(1, 1, 4);
		grid.setNumber(3, 1, 4);
		
		grid.setNumber(0, 2, 8);
		grid.setNumber(2, 2, 8);
		grid.setNumber(3, 2, 4);
		
		grid.setNumber(0, 3, 16);
		grid.setNumber(3, 3, 16);
	}
	
	private void createDummy2() {
		grid.setNumber(0, 0, 4);
		grid.setNumber(0, 1, 2);
		grid.setNumber(0, 2, 2);
		
		grid.setNumber(1, 1, 4);
		grid.setNumber(1, 3, 4);
		
		grid.setNumber(2, 0, 8);
		grid.setNumber(2, 2, 8);
		grid.setNumber(2, 3, 4);
		
		grid.setNumber(3, 0, 16);
		grid.setNumber(3, 3, 16);
	}
	
	@Test
	public void testLeftMove() {
		createDummy1();
		rules.processCommand(Command.LEFT, 0);
		Assert.assertEquals(4, grid.getNumber(0, 0));
		Assert.assertEquals(4, grid.getNumber(1, 0));
		Assert.assertEquals(0, grid.getNumber(2, 0));
		Assert.assertEquals(0, grid.getNumber(3, 0));
		
		Assert.assertEquals(8, grid.getNumber(0, 1));
		Assert.assertEquals(0, grid.getNumber(1, 1));
		Assert.assertEquals(0, grid.getNumber(2, 1));
		Assert.assertEquals(0, grid.getNumber(3, 1));
		
		Assert.assertEquals(16, grid.getNumber(0, 2));
		Assert.assertEquals(4, grid.getNumber(1, 2));
		Assert.assertEquals(0, grid.getNumber(2, 2));
		Assert.assertEquals(0, grid.getNumber(3, 2));
		
		Assert.assertEquals(32, grid.getNumber(0, 3));
		Assert.assertEquals(0, grid.getNumber(1, 3));
		Assert.assertEquals(0, grid.getNumber(2, 3));
		Assert.assertEquals(0, grid.getNumber(3, 3));
		
		Assert.assertEquals(60, rules.getScore());
	}
	
	@Test
	public void testRightMove() {
		createDummy1();
		rules.processCommand(Command.RIGHT, 0);
		Assert.assertEquals(0, grid.getNumber(0, 0));
		Assert.assertEquals(0, grid.getNumber(1, 0));
		Assert.assertEquals(4, grid.getNumber(2, 0));
		Assert.assertEquals(4, grid.getNumber(3, 0));
		
		Assert.assertEquals(0, grid.getNumber(0, 1));
		Assert.assertEquals(0, grid.getNumber(1, 1));
		Assert.assertEquals(0, grid.getNumber(2, 1));
		Assert.assertEquals(8, grid.getNumber(3, 1));
		
		Assert.assertEquals(0, grid.getNumber(0, 2));
		Assert.assertEquals(0, grid.getNumber(1, 2));
		Assert.assertEquals(16, grid.getNumber(2, 2));
		Assert.assertEquals(4, grid.getNumber(3, 2));
		
		Assert.assertEquals(0, grid.getNumber(0, 3));
		Assert.assertEquals(0, grid.getNumber(1, 3));
		Assert.assertEquals(0, grid.getNumber(2, 3));
		Assert.assertEquals(32, grid.getNumber(3, 3));
		
		Assert.assertEquals(60, rules.getScore());
	}
	
	
	@Test
	public void testUpMove() {
		createDummy2();
		rules.processCommand(Command.UP, 0);
		Assert.assertEquals(4, grid.getNumber(0, 0));
		Assert.assertEquals(8, grid.getNumber(1, 0));
		Assert.assertEquals(16, grid.getNumber(2, 0));
		Assert.assertEquals(32, grid.getNumber(3, 0));
		
		Assert.assertEquals(4, grid.getNumber(0, 1));
		Assert.assertEquals(0, grid.getNumber(1, 1));
		Assert.assertEquals(4, grid.getNumber(2, 1));
		Assert.assertEquals(0, grid.getNumber(3, 1));
		
		Assert.assertEquals(0, grid.getNumber(0, 2));
		Assert.assertEquals(0, grid.getNumber(1, 2));
		Assert.assertEquals(0, grid.getNumber(2, 2));
		Assert.assertEquals(0, grid.getNumber(3, 2));
		
		Assert.assertEquals(0, grid.getNumber(0, 3));
		Assert.assertEquals(0, grid.getNumber(1, 3));
		Assert.assertEquals(0, grid.getNumber(2, 3));
		Assert.assertEquals(0, grid.getNumber(3, 3));
		
		Assert.assertEquals(60, rules.getScore());
	}
	
	@Test
	public void testDownMove() {
		createDummy2();
		rules.processCommand(Command.DOWN, 0);
		Assert.assertEquals(0, grid.getNumber(0, 0));
		Assert.assertEquals(0, grid.getNumber(1, 0));
		Assert.assertEquals(0, grid.getNumber(2, 0));
		Assert.assertEquals(0, grid.getNumber(3, 0));
		
		Assert.assertEquals(0, grid.getNumber(0, 1));
		Assert.assertEquals(0, grid.getNumber(1, 1));
		Assert.assertEquals(0, grid.getNumber(2, 1));
		Assert.assertEquals(0, grid.getNumber(3, 1));
		
		Assert.assertEquals(4, grid.getNumber(0, 2));
		Assert.assertEquals(0, grid.getNumber(1, 2));
		Assert.assertEquals(16, grid.getNumber(2, 2));
		Assert.assertEquals(0, grid.getNumber(3, 2));
		
		Assert.assertEquals(4, grid.getNumber(0, 3));
		Assert.assertEquals(8, grid.getNumber(1, 3));
		Assert.assertEquals(4, grid.getNumber(2, 3));
		Assert.assertEquals(32, grid.getNumber(3, 3));
		
		Assert.assertEquals(60, rules.getScore());
	}
}
