package com.mi.game;

import java.util.Scanner;

public class Input {
	private Scanner input = new Scanner(System.in);
	
	public Command getCommand() {
		char keyPressed = input.next().charAt(0);
		return getCommand(keyPressed);
	}
	
	public Command getCommand(char keyPressed) {
		switch(keyPressed) {
		case 'a':
			return Command.LEFT;
		case 'd':
			return Command.RIGHT;
		case 'w':
			return Command.UP;
		case 's':
			return Command.DOWN;
		case 'q':
			return Command.QUIT;
		default:
			return Command.NOTHING;
		}
	}
}
