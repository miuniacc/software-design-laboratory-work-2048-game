package com.mi.test;

import org.junit.Before;
import org.junit.Test;

import com.mi.game.Command;
import com.mi.game.Grid;
import com.mi.game.Rules;

import junit.framework.Assert;

public class TestEndGameCondition {
	
	private Grid grid = new Grid();
	private Rules rules = new Rules(grid);
	
	@Before
	public void clearGrid() {
		for(int x = 0; x < 4; x++) {
			for(int y = 0; y < 4; y++) {
				grid.setNumber(x, y, 0);
			}
		}
	}
	
	private void createDummy1() {
		grid.setNumber(0, 0, 2);
		grid.setNumber(1, 0, 4);
		grid.setNumber(2, 0, 2);
		grid.setNumber(3, 0, 4);
		
		grid.setNumber(0, 1, 4);
		grid.setNumber(1, 1, 2);
		grid.setNumber(2, 1, 4);
		grid.setNumber(3, 1, 2);
		
		grid.setNumber(0, 2, 2);
		grid.setNumber(1, 2, 4);
		grid.setNumber(2, 2, 2);
		grid.setNumber(3, 2, 4);
		
		grid.setNumber(0, 3, 4);
		grid.setNumber(1, 3, 2);
		grid.setNumber(2, 3, 4);
		grid.setNumber(3, 3, 2);
	}
	
	private void createDummy2() {
		grid.setNumber(0, 0, 2);
		grid.setNumber(1, 0, 2);
		grid.setNumber(2, 0, 2);
		grid.setNumber(3, 0, 4);
		
		grid.setNumber(0, 1, 4);
		grid.setNumber(1, 1, 2);
		grid.setNumber(2, 1, 4);
		grid.setNumber(3, 1, 2);
		
		grid.setNumber(0, 2, 2);
		grid.setNumber(1, 2, 4);
		grid.setNumber(2, 2, 2);
		grid.setNumber(3, 2, 4);
		
		grid.setNumber(0, 3, 4);
		grid.setNumber(1, 3, 2);
		grid.setNumber(2, 3, 4);
		grid.setNumber(3, 3, 2);
	}
	
	@Test
	public void testGameOver() {
		createDummy1();
		Assert.assertTrue(rules.isGameOver());
		createDummy2();
		Assert.assertFalse(rules.isGameOver());
	}
	
}
