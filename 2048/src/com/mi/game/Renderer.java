package com.mi.game;

public class Renderer {
	
	private Grid grid;
	
	public Renderer(Grid grid) {
		this.grid = grid;
	}
	
	public void render(int turn, int score) {
		System.out.println("Turn: " + turn + " Score: " + score);
		System.out.println("#################################");
		for(int y = 0; y < 4; y++) {
			System.out.println("#       #       #       #       #");
			for(int x = 0; x < 4; x++) {
				if(grid.isFree(x, y)) {
					System.out.print("#       ");
				}
				else {
					System.out.printf("# %5d ", grid.getNumber(x, y));
				}
			}
			System.out.print("#\n");
			System.out.println("#       #       #       #       #");
			System.out.println("#################################");
		}
	}
}
