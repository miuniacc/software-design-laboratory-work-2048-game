package com.mi.game;

public class Main {

	public static void main(String[] args) {
		
		int score = 0;
		int turn = 1;
		
		Grid grid = new Grid();
		Renderer renderer = new Renderer(grid);
		Generator generator = new Generator(grid);
		Rules rules = new Rules(grid);
		Input input = new Input();
		
		generator.generate(turn);
		renderer.render(turn, score);
		
		while(true) {
			if(rules.isGameOver()) {
				rules.endTheGame();
			}
			
			Command command = input.getCommand();
			
			rules.processCommand(command, score);
			
			score = rules.getScore();
			
			if(rules.goalReached()) {
				rules.win();
			}
			
			if(rules.gridChanged()) {
				turn++;
				generator.generate(turn);
				renderer.render(turn, score);
			}
			else {
				System.out.println("Invalid move.");
			}
		}
	}

}
